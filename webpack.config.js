const path = require('path');

module.exports = {
  entry: {
    bundle: './staticfiles/src/index.js',
  },

  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './staticfiles/dist'),
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },

      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },

      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader',
        ],
      },
    ],
  },

  devServer: {
    hot: true,
    publicPath: '/static/',
    port: 3000,
    proxy: [
      {
        context: [
          '**',
          '!/static/**',
          '!/media/**'
        ],
        target: 'http://localhost:8000',
        changeOrigin: true,
      },
    ],
  },
};
