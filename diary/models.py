from django.db import models

# Create your models here.
class DiaryRecords(models.Model):

    class Meta:
        db_table = 'diary_records'

    place = models.CharField('Place',max_length=255)
    date =  models.DateField('Start Date')
    time_from = models.TimeField('Starting Time', null=True, blank=True)
    time_to = models.TimeField('Ending Time', null=True, blank=True)
    purpose = models.CharField('Purpose', max_length=500)
#    urls = models.URLField('Blog URL',null=True,blank=True)
    notes = models.TextField('Notes', null=True, blank=True)
#    photo_upload = models.ImageField('Photo Upload', upload_to="diary/photo", null=True, blank=True)
#    photo_name = models.CharField('Photo Name',max_length=255,null=True,blank=True)
