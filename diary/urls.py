from django.urls import path # calls path from django.urls module

from .views import DataSearchView, DataAddView, DataEditView, DataDetailsView, DataDeleteView, DataSearchJSON  # view is imported

urlpatterns = [
            path('', DataSearchView.as_view(), name='search-view'),
            path('datalist/', DataSearchJSON.as_view(), name='datatable-view'),
            path('add/', DataAddView.as_view(), name='add-view'),
            path('<int:pk>/edit', DataEditView.as_view(), name='edit-view'),
            path('<int:pk>/details', DataDetailsView.as_view(), name='details-view'),
            path('<int:pk>/delete', DataDeleteView.as_view(), name='delete-view'),
            ]
