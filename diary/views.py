from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from diary.models import DiaryRecords
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from django.utils.html import format_html
from django_datatables_view.base_datatable_view import BaseDatatableView
# Create your views here.

class DataSearchView(ListView):
    model = DiaryRecords
    template_name = 'data_list.html'

class DataSearchJSON(BaseDatatableView):
    model = DiaryRecords
    columns = ["slno","place","date","purpose","time_from","time_to","id"]

    def render_column(self, row, column):
        if column == 'id':
            return format_html('<a href="{}/details" class="btn btn-sm btn-success" type="submit" name="submit" value="view"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-eye" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><circle cx="12" cy="12" r="2" /><path d="M2 12l1.5 2a11 11 0 0 0 17 0l1.5 -2" /><path d="M2 12l1.5 -2a11 11 0 0 1 17 0l1.5 2" /></svg>View</a>  <a href="{}/edit" class="btn btn-sm btn-warning" type="submit" name="submit" value="edit"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-edit" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><path d="M9 7 h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3" /><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3" /><line x1="16" y1="5" x2="19" y2="8" /></svg>Edit</a> <a href="{}/delete" class="btn btn-sm btn-warning" type="submit" name="submit" value="edit"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-edit" width="44" height="44" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z"/><path d="M9 7 h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3" /><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3" /><line x1="16" y1="5" x2="19" y2="8" /></svg>Delete</a>',row.id,row.id,row.id)
        else:
            return super(DataSearchJSON, self).render_column(row, column)

class DataAddView(CreateView):
    model = DiaryRecords
    template_name = 'data_add.html'
    fields = "__all__"

    def get_success_url(self):
        return reverse('search-view')

class DataEditView(DataAddView, UpdateView):
    pass

class DataDetailsView(DetailView):
    model = DiaryRecords
    template_name = 'data_details.html'

class DataDeleteView(DeleteView):
    model = DiaryRecords
    template_name = 'data_delete.html'

    def get_success_url(self):
        return reverse('search-view')
